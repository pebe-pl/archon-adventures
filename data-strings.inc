; you can use this file to define all your strings in one file 
; it's always useful to do that if you plan to translate your work later

strings_list
    dta a(txt_1)
	dta a(txt_2)
	dta a(txt_3)
	dta a(txt_4)
	dta a(txt_5)
	dta a(txt_6)
	dta a(txt_7)
	dta a(txt_8)
	
strings

txt_1
    .SB 'PRESS ESC TO EXIT...' $FF
txt_2
    .SB 'USE JOYSTICS TO MOVE HERO...' $FF
txt_3
	.SB 'EQUIPMENT' $FF
txt_4
	.SB 'ARMAMENT' $FF
txt_5
	.SB 'HEALTH' $FF
txt_6
	.SB 'MAGIC' $FF
txt_7
	.SB 'DEXTERITY' $FF
txt_8
	.SB 'STRENGTH' $FF
	
 .print "* Defined strings size: ", *-strings_list
 .print "* Strings : ", strings_list, "..", * 
