program ArchonAdventure;
{$librarypath '../../blibs/'}
uses atari, b_pmg
// {$DEFINE debug_mode}

{$IFDEF debug_mode}
 ,b_crt
{$ENDIF}
;
{$I-}

const
{$i const.inc}
{$i chartype.inc}
{$r resources.rc}
{$i interrupts.inc}

Const
	scr_width = 32;	// Screen line width
	
	shx = $3e;	// Shift player position x to screen 
	shy = $1b;	// Shift player position y to screen

Const
	LightMap:Array[0..6,0..7] of byte =
	(( $0,  $0,  $0, $80, $80,  $0,  $0,  $0),
	 ( $0,  $0, $80, $80, $80, $80,  $0,  $0),
	 ( $0, $80, $80, $80, $80, $80, $80,  $0),
	 ($80, $80, $80, $80, $80, $80, $80, $80),
	 ( $0, $80, $80, $80, $80, $80, $80,  $0),
	 ( $0,  $0, $80, $80, $80, $80,  $0,  $0),
	 ( $0,  $0,  $0, $80, $80,  $0,  $0,  $0));

var
    //msx: TRMT;
    pmgram:array[0..7,0..255] of byte	absolute PMG_RAM_ADDRESS;		// memory area for Players Missiles Graphics
	buf:array[0..scr_width-1] of byte	absolute PMG_RAM_ADDRESS;		// screen buffer
	scr:array[0..0] of byte				absolute VIDEO_RAM_ADDRESS;		// memory area for screen
	scradr:array[0..25] of word;										// map of screen lines addresses
	
    rawdata:array[0..0] of pointer		absolute RAWDATA_ADDRESS;		// pointer array for raw data (ex. screen elements)
    pmgdata:array[0..0] of pointer		absolute PMGDATA_ADDRESS;		// pointer array for player graph shapes

    map_width, map_height:byte;											// map size
    map_sx, map_sy:byte;												// map offset
    map:array[0..0] of byte			absolute MAPDATA_ADDRESS;		// memory area for map
    
//    wsync:byte absolute $d40a;
//    trig0:byte absolute $d010;
    joy0:byte absolute $278;	// joystick port 0 register
	stick:byte;		// Joystick "internal shadow"
    kbcode:byte absolute $2FC; // Keyboard key code
    keycode:byte;		// Keyboard key "internal shadow"

// In game varibles
//

Var
	pdx,pdy:shortint;	// relative value of the hero's direction of movement
	p0x,p0y,			// hero position x/y (Player 0-1)
	p0d,				// definition of a sprite for a hero (data_pmg.warrior)
	p0sw:byte;			// hero animation frame switch
	sx,sy:byte;			// auxiliary variables of the screen position
//	osx,osy:byte;		// previous screen position values
	oscr_adr,			// previous screen position address
//	sa:word;			// screen position address
	scr_adr:word;		// help varible to calculate screen position
	omap_adr,			// previous map address
	map_adr:word;		// help varible to calculate map address
	ppf0,ppf1,			// previous characters
	pf0,pf1:byte;		// characters under the hero's feet
	cnt:byte;			// loop counter

	ev_Back:Boolean;	// back event inicator
	pct0,pct1,			// previous character flags
	ct0,ct1:byte;		// actual character flags
{$IFDEF debug_mode}
	ev_test:boolean;	// Debug only: events pass
{$ENDIF}

//
//

Procedure VWait(); assembler;
asm {
	lda $d40b
	bmi *-3
	lda $d40b
	bpl *-3
};
End;

function Bit(b:byte; bit:byte):boolean;
Begin
	result:=(b and bit)=bit;
End;

//
//

Procedure Init();
Begin
    savmsc := VIDEO_RAM_ADDRESS;  // set custom video address
    for cnt:=0 to 25 do scradr[cnt]:=cnt*scr_width; // prepare lines addresses map
    
{$IFDEF debug_mode}
    CRT_Init(savmsc,scr_width,26);
{$ENDIF}

(*  set custom display list  *)
//    Pause;
    SDLSTL := DISPLAY_LIST_ADDRESS;

(*	initialise PMG *)
	PMG_pmbase:=hi(PMG_RAM_ADDRESS);
	
	PMG_sizep0:=	%00000000; 		// player 1 normal size
	PMG_sizep1:=	%00000000;		// player 2 normal size
	PMG_gractl:=	%00000011; 		// players on
	PMG_sdmctl_S:=	%00000001 or	// narrow screen
					%00001100 or	// players on
					%00010000 or	// one-line resolution
					%00100000;		// DirectMemoryAccess
			
	PMG_gprior_S:=	%00100000 or	// Overlap of players have 3rd color
					%00000001;		// Priority set to Player 0-3 / playfield color 0-3 / Background
	
	PMG_pcolr0_S:=$12; PMG_pcolr1_S:=$14;
	
(*	clear PMG RAM *)
	fillchar(@pmgram,2048,$00);
End;

Procedure Done();
Begin
	RestoreDLIInterrupt;
End;

//
//
Var
	scr_ln,		// screen line counter
	light_sln,	// light start line
	light_sx,	// light x start
	light_bi,	// light map start
	light_len,	// light length
	light_x,
	light_y,
	i,			// character in line counter
	ch:byte;	// character code

Procedure DrawMap;
Begin
	map_adr:=map_sy*map_width+map_sx;
	scr_adr:=scradr[2];
	
	if shortint(sy-3)>=0 then
	Begin
		light_sln:=sy-3; light_y:=0;
	End
	else
	Begin
		light_sln:=0; light_y:=3-sy;
	End;
	
	light_len:=8;
	if shortint(sx-3)>=0 then
	Begin
		light_sx:=sx-3; light_bi:=0;
	End
	else
	Begin
		light_sx:=0; light_bi:=3-sx;
	End;
	
	for scr_ln:=0 to 15 do
	Begin
		move(@map[map_adr],@buf,scr_width); // copy one line of map to buffer
		
		for i:=0 to scr_width-1 do
		Begin
			ch:=buf[i];
			if (ch and $80=$80) then
				buf[i]:=ch and $7f
			else
				buf[i]:=0;
		End;

		if (scr_ln>=light_sln) and (light_y<=6) then // draw light when, 'screen line' is equal or more than 'light start line'
		Begin
			light_x:=light_sx;
			i:=light_bi;
			Repeat
				buf[light_x]:=buf[light_x] or LightMap[light_y,i];
				inc(light_x); inc(i);
			Until i=light_len;
			inc(light_y);
		End;
		
		move(@buf,@scr[scr_adr],scr_width); // put buffer to screen
		inc(map_adr,map_width);
		inc(scr_adr,scr_width);
	End;
End;

//
//
Procedure UnhideMap(x,y:byte);
Var
  c,xx,yy:byte;
  light_gap:byte;
  
Begin
	light_len:=7;

	if (shortint(x-3)<0) then
	Begin	// cuting light parameters in x axis for left margin
		xx:=0; light_bi:=3-x;
	End
	else
	Begin
		xx:=x-3; light_bi:=0;
	End;
	if (shortint(y-3)<0) then
	Begin	// cuting light parameters in y axis for top margin
		yy:=0; light_y:=3-y;
	End
	else
	Begin
		yy:=y-3; light_y:=0;
	End;
	map_adr:=yy*map_width+xx;
	light_gap:=map_width-light_len+light_bi-1;

	if (xx+8)>=map_width then
	Begin // cuting light parameters in x axis for right margin
		light_len:=map_width-(xx+1);
		inc(light_gap,7-light_len);
	End;
	
	for yy:=light_y to 6 do
	Begin
		i:=light_bi;
		Repeat
			map[map_adr]:=map[map_adr] or LightMap[yy,i];
			inc(map_adr); inc(i);
		Until i>light_len;
		inc(map_adr,light_gap);
	end;
End;

Procedure SetLight(x,y:byte);
Var
  c,xx,yy:byte;

Begin
	scr_adr:=scradr[y-1]+(x-3);
	for yy:=0 to 6 do
	Begin
		for xx:=0 to 7 do
		Begin
			if LightMap[yy,xx]<>0 then
			Begin
				c:=scr[scr_adr];
				scr[scr_adr]:=c or $80;
			End;
			inc(scr_adr);
		End;
		inc(scr_adr,scr_width-8);
	end;
End;

//
//
Procedure SetGameScreen;
Begin
	setDLIInterrupts;
	move(rawdata[0],@scr[0],64);		// put top status bar
    move(rawdata[1],@scr[18*32],32);	// put top decor line in interface
    move(rawdata[2],@scr[25*32],32);	// put bottom decor line in interface
    
    map_width:=32; map_height:=32;
    map_sx:=0; map_sy:=0;
//	move(mapdata,@scr[64],16*32);		// put map on screen
	DrawMap;
end;

//
//
var
	op0y:byte;	// previous player Y position
	p0data:byte;	// player definition data
	
Procedure SetPlayerPos(x,y:byte);
Begin
	p0data:=p0data shl 1;
	if y<>op0y then
	Begin
		fillchar(@pmgram[4,op0y],16,0);
		fillchar(@pmgram[5,op0y],16,0);
	End;
	move(pmgdata[p0data],@pmgram[4,y],16);
	move(pmgdata[p0data+1],@pmgram[5,y],16);
	PMG_hpos0:=x;
	PMG_hpos1:=x;
	op0y:=y;
	p0data:=p0data shr 1;
End;

{$IFDEF debug_mode}
procedure debug_ct(ct,pf:byte; x,y:byte; s:string);
Begin
	crt_gotoxy(x,y); crt_write(s);
	crt_write(atascii2antic(HexStr(pf0,2)));
	crt_write(char($00));
	if boolean(ct and ct_fPass) then crt_write(char($B0)) else crt_write(char($30));
	if boolean(ct and ct_fSet) then crt_write(char($33)) else crt_write(char($0d));
	if boolean(ct and ct_fActive) then crt_write(char($21)) else crt_write(char($0d));
	if boolean(ct and ct_stairs) then crt_write(char($33)) else crt_write(char($0d));
	if boolean(ct and ct_wall) then crt_write(char($37)) else crt_write(char($0d));
	if boolean(ct and ct_window) then crt_write(char($37)) else crt_write(char($0d));
	if boolean(ct and ct_door) then crt_write(char($24)) else crt_write(char($0d));
	if boolean(ct and ct_item) then crt_write(char($29)) else crt_write(char($0d));
End;
{$ENDIF}

//
//

Function gpriorCheck():boolean;
Begin
	ct0:=CharTypeTab[pf0];	// ct0:=debugCharType(pf0);
	ct1:=CharTypeTab[pf1];	// ct1:=debugCharType(pf1);
{$IFDEF debug_mode}
//	crt_gotoxy(13,22);
//	crt_write('GPRIOR'~);
//	debug_ct(ct0,pf0,0,23,'PF0 '~);
//	debug_ct(ct1,pf1,16,23,'PF1 '~);
{$ENDIF}
	if ((pf0=0) and (pf1=0)) then
	Begin
//		if	(Not bit(ct0,ct_Wall) or Not bit(ct1,ct_Wall)) then
		Begin
			PMG_gprior_S:=%00100001;		// set Hero above playfield
											// %00100000 Overlap of players have 3rd color
											// %00000001 Priority set to Player 0-3 / playfield color 0-3 / Background
			result:=true;
			{$IFDEF debug_mode}
//			crt_gotoXY(20,22); crt_write('UNDER'*~);
			{$ENDIF}
			exit;
		End;
	End;
	PMG_gprior_S:=%00100100;			// set Hero under playfield
				  						// %00100000 Overlaps of players have 3rd color;
										// %00000100 Priority set to Playfield color 0-3 / Player 0-3 / Background
	{$IFDEF debug_mode}
//	crt_gotoXY(20,22); crt_write('ABOVE'*~);
	{$ENDIF}
	result:=false;
End;

// Main program block
//
	
begin
	Init;
	
	SetGameScreen;

// resetowanie wartości
	p0x:=116; p0y:=104; p0data:=0; p0sw:=0;
	SetPlayerPos(p0x,p0y);
	sx:=(p0x-shx) shr 2;	// calculate screen position x
	sy:=(p0y-shy) shr 3;	// calculate screen position y
	UnhideMap(map_sx+sx,map_sy+sy);
	DrawMap;
	SetLight(sx,sy);
	oscr_adr:=0;
//	osx:=sx; osy:=sy;
	cnt:=5;
	stick:=0;
	kbcode:=255;
{$IFDEF debug_mode}
	ev_test:=true;
{$ENDIF}
	
// Main loop
//
	repeat
		if stick=0 then stick:=joy0 xor 15;
		if (stick<>0) and (cnt=5) then
		Begin
			pdx:=0; pdy:=0;	// clear directions indicator
			ev_Back:=false;	// clear Back-Move event
			
			if (stick=%0100) then Begin p0d:=2; pdx:=-1; pdy:=0; End;	// left
			if (stick=%1000) then Begin p0d:=0; pdx:=1; pdy:=0; End;	// right
			if (stick=%0001) then Begin pdx:=0; pdy:=-1; End;	// up
			if (stick=%0010) then Begin pdx:=0; pdy:=1; End;	// down

			if (pdx<>0) or (pdy<>0) then
			Begin
				cnt:=1;	// set counter to enable player move
				p0data:=p0d+p0sw; SetPlayerPos(p0x,p0y);
			End
			else
			Begin
				Stick:=0;
			End;

// events
//

 			sx:=((p0x-shx) shr 2)+pdx;	// calculate next position x
			sy:=((p0y-shy) shr 3)+pdy;	// calculate next position y

			map_adr:=(map_sy+sy)*map_width+(map_sx+sx);	// calculate address on map
			
			if (pdy=0) then
			Begin	// only for horizontal move
				if (pdx<0) then
				Begin 
					pf0:=map[map_adr];
					dec(map_adr,map_width);
					pf1:=map[map_adr];
				End;
				if (pdx>0) then
				Begin
					inc(map_adr);
					pf0:=map[map_adr];
					dec(map_adr,map_width);
					pf1:=map[map_adr];
				End;
				pf0:=pf0 and $7f; pf1:=pf1 and $7f; // remove 8bit (invers)
				
				ct1:=CharTypeTab[pf1];	// ct1:=debugCharType(pf1);
				ct0:=CharTypeTab[pf0];	// ct0:=debugCharType(pf0);
{$IFDEF debug_mode}
//				debug_ct(ct1,pf1,16,19,'C1 '~);
//				debug_ct(ct0,pf0,0,19,'C0 '~);
//				crt_gotoxy(14,20); crt_write('PCH: '~);
//				crt_gotoxy(0,21); crt_write('                                '~);
{$ENDIF}
				// ct1 - top character
				// ct0 - bottom character
// event test
//
				if bit(ct0,ct_fPass) then
				Begin
					ev_Back:=true;
					if not bit(ct1,ct_fPass) and not bit(ct1,ct_wall) then ev_Back:=false;
					if not bit(ct1,ct_fPass) and bit(ct1,ct_door) then ev_Back:=false;
					if bit(ct1,ct_fPass) and ((pf1>$3f) and (pf1<$43)) then ev_Back:=false;
					if bit(ct0,ct_Stairs) then ev_Back:=true;
					if not bit(ct0,ct_Stairs) and bit(ct1,ct_Stairs) then ev_Back:=true;
				End;
				if ( bit(ct0,ct_wall) and bit(ct1,ct_wall) ) then
				Begin
					if (not bit(ct0,ct_fPass) and (pf1=$3f)) then ev_Back:=true;
				End;
			End
			else
			Begin	// only for vertical move
				if (pdy<0) then
				Begin 
					pf0:=map[map_adr];
					inc(map_adr);
					pf1:=map[map_adr];
					dec(map_adr,map_width+1);
					ppf0:=map[map_adr];
					inc(map_adr);
					ppf1:=map[map_adr];
				End;
				if (pdy>0) then
				Begin
					pf0:=map[map_adr];
					inc(map_adr);
					pf1:=map[map_adr];
					dec(map_adr,map_width+1);
					ppf0:=map[map_adr];
					inc(map_adr);
					ppf1:=map[map_adr];
				End;
				ppf0:=ppf0 and $7f; ppf1:=ppf1 and $7f; // remove 8bit (invers)
				pf0:=pf0 and $7f; pf1:=pf1 and $7f; // remove 8bit (invers)

				ct0:=CharTypeTab[pf0];	// ct0:=debugCharType(pf0);
				ct1:=CharTypeTab[pf1];	// ct1:=debugCharType(pf1);
				pct0:=CharTypeTab[ppf0];	// pct0:=debugCharType(ppf0);
				pct1:=CharTypeTab[ppf1];	// pct1:=debugCharType(ppf1);
{$IFDEF debug_mode}
//				debug_ct(ct0,pf0,0,19,'C0 '~);
//				debug_ct(ct1,pf1,16,19,'C1 '~);
//				crt_gotoxy(14,20); crt_write('PCH: '*~);
//				debug_ct(pct0,ppf0,0,21,'C0 '~);
//				debug_ct(pct1,ppf1,16,21,'C1 '~);
{$ENDIF}
				// ct1 - left character
				// ct0 - right character
// events
//
				if ( bit(ct0,ct_fPass) or bit(ct1,ct_fPass) ) then
				Begin
					ev_Back:=true;
					if	(not bit(pct0,ct_Wall) and not bit(pct1,ct_Wall)) and
						(not bit(pct0,ct_Door) and not bit(pct1,ct_Door)) and 
						(not bit(pct0,ct_Stairs) and not bit(pct1,ct_Stairs)) then ev_Back:=false;
				End;
				if (not bit(ct0,ct_fPass) and not bit(ct1,ct_fPass)) then
				Begin
				  ev_Back:=false;
				End;
			End;

//
//
		end;

{$IFDEF debug_mode}
			crt_gotoxy(0,24);
			if ev_test then
				crt_write('EVENT: '~*)
			else
				crt_write('EVENT: '~);
			if ev_Back then
				crt_write(' BACK'~)
			else
				crt_write('     '~);
			if not ev_test then ev_Back:=false; // event test?
{$ENDIF}

			if ev_Back then
			Begin
				pdx:=0; pdy:=0; // clear direction
				cnt:=5; // set counter to no animated and move players
				stick:=0; // reset stick shadow to enable check read joystic state
				ev_Back:=false; // reset event indicator
			End;

		if (kbcode<>255) and (skstat and 4<>0) then // KeyPress detect. Runs only if keyup.
		Begin
			keycode:=kbcode;
			kbcode:=255;
			{$IFDEF debug_mode}
			crt_gotoxy(30,24); crt_write(atascii2antic(HexStr(keycode,2)));
			{$ENDIF}
				{$IFDEF debug_mode}
				case keycode of
					$2a: Begin // "E" keyboard code; switch event test
							ev_test:=not ev_test;
						 End;
				End;
				{$ENDIF}
		End;

		if (cnt>0) and (cnt<5) then
		Begin
			p0sw:=1-p0sw; // switch animation frame
			inc(p0x,pdx); inc(p0y,pdy shl 1);			// move player
			p0data:=p0d+p0sw; SetPlayerPos(p0x,p0y);
			sx:=(p0x-shx) shr 2;	// calculate screen position x
			sy:=(p0y-shy) shr 3;	// calculate screen position y
			map_adr:=(map_sy+sy)*map_width+(map_sx+sx);	// calculate address on map
			
			if map_adr<>omap_adr then // update only if position change
			Begin
				omap_adr:=map_adr; // set previous value of player screen position
				pf0:=map[map_adr] and $7f; inc(map_adr); pf1:=map[map_adr] and $7f; // get playfiled characters to identify GPrior register set

				UnhideMap(map_sx+sx,map_sy+sy);
				VWait;
				DrawMap;
//				SetLight(sx,sy);
				GPriorCheck;	// identify by pf0-1 characters, how player is visible on screen

			End;
			inc(cnt);
			pause(4);
			if cnt=5 then stick:=0;
		End;
	until peek($d209)=28;
    Done;
end.
