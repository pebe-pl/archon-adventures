;
;

pmgdata_list
	; right
	dta a(warrior_0a)
	dta a(warrior_0b)
	dta a(warrior_1a)
	dta a(warrior_1b)
	; left
	dta a(warrior_2a)
	dta a(warrior_2b)
	dta a(warrior_3a)
	dta a(warrior_3b)
    
pmgdata

warrior_0a
	.HE 00 00 00 38 3a 1a 3e 7e 7a 7f 00 00 00 00 00 00
warrior_0b
	.HE 00 5a 3c 08 02 02 02 02 02 3f 1c 14 34 24 36 00
warrior_1a
	.HE 00 00 00 38 3a 1e 1e 3f 2e 38 00 00 00 00 00 00
warrior_1b
	.HE 00 5a 3c 08 02 02 02 03 0e 10 1c 18 14 18 1c 00

warrior_2a
	.HE 00 00 00 1c 5c 58 7c 7e 5e fe 02 00 00 00 00 00
warrior_2b
	.HE 00 5a 3c 10 40 40 40 40 40 fc 38 28 2c 24 6c 00
warrior_3a
	.HE 00 00 00 1c 5c 78 78 fc 74 1c 00 00 00 00 00 00
warrior_3b
	.HE 00 5a 3c 10 40 40 40 c0 70 08 38 18 28 18 38 00
	
 .print "* Defined PMG data : ", *-pmgdata
 .print "* PMG data: ", pmgdata, "..", *-pmgdata_list
