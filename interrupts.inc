(* declare your interrupt routines here *)

var
	reg_a:byte absolute $80;
//    oldvbl,
    oldsdli:pointer;
	
procedure dli_game1; assembler; interrupt;
asm {
		sta reg_a

		lda #<dli_game2
		sta vdslst
		lda #>dli_game2
		sta vdslst+1
		
chg1	lda #>CHARSET1_ADDRESS
		sta wsync
		sta atari.chbase
		lda #$32
		sta atari.colpf0
		lda #$3a
		sta atari.colpf1
		lda #$34
		sta atari.colpf2
		lda #$30
		sta atari.colpf3
		
		lda reg_a
};
end;

procedure dli_game2; assembler; interrupt;
asm {
		sta reg_a

		lda #<dli_game3
		sta vdslst
		lda #>dli_game3
		sta vdslst+1

chg2	lda #>CHARSET2_ADDRESS
		sta wsync
		sta atari.chbase
		lda #$1b
		sta atari.colpf0
		lda #$16
		sta atari.colpf1
		lda #$12
		sta atari.colpf2
		lda #$14
		sta atari.colpf3

		lda reg_a
};
end;

procedure dli_game3; assembler; interrupt;
asm {
		sta reg_a

		lda #<dli_game1
		sta vdslst
		lda #>dli_game1
		sta vdslst+1
		
chg3	lda #>CHARSET3_ADDRESS
		sta wsync
		sta atari.chbase
		lda #$1b
		sta atari.colpf0
		lda #$16
		sta atari.colpf1
		lda #$12
		sta atari.colpf2
		lda #$14
		sta atari.colpf3

		lda reg_a
};
end;

procedure setDLIInterrupts;
Begin
(*  set and run display list interrupts *)
    GetIntVec(iDLI, oldsdli);
    SetIntVec(iDLI, @dli_game1);
    nmien := $c0; // set $80 for dli only (without vbl) old $c0
End;

Procedure RestoreDLIInterrupt;
Begin
(*  restore system interrupts *)
//    SetIntVec(iVBL, @oldvbl);
    nmien := $40; // turn off dli
    SetIntVec(iDLI, oldsdli);
End;
