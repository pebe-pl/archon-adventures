// Character flags
//
const
	ct_fPass	:byte = %10000000;	// Back flag
	ct_fSet  	:byte = %01000000;	// Set flag: door-open/close;
	ct_fActive	:byte = %00100000;	// Active element flag
	
	ct_stairs	:byte = %00010000;	// Detected: stairs
	ct_wall		:byte = %00001000;	// Detected: wall
	ct_window	:byte = %00000100;	// Detected: window
	ct_door		:byte = %00000010;	// Detected: door
	ct_item		:byte = %00000001;	// Detected: item
	
// Character flags table
//
const
	charTypeTab:array[0..127] of byte=(
	// $00-$0f
	0,										// $00 // ** free space !!
	0,										// $01 // Tree
	0,										// $02 // Tree
	0,										// $03 // Tree
	0,										// $04 // Tree
	0,										// $05 // Tree
	0,										// $06 // Tree
	0,										// $07 // Tree
	0,										// $08 // Tree
	0,										// $09 // Plant
	0,										// $0a // Plant
	0,										// $0b // Plant
	0,										// $0c // Plant
	0,										// $0d // Plant
	0,										// $0e // Grass
	0,										// $0f // Grass

	// $10-$1f
	0,										// $10 // Grass
	0,										// $11 // Grass
	0,										// $12 // Grass
	0,										// $13 // Grass
	0,										// $14 // Grass
	0,										// $15 // Grass
	ct_Door+ct_Window,						// $16 Door - Opened
	ct_Door+ct_Window,						// $17 Door - Opened
	ct_Window,								// $18 Window
	ct_Window,								// $19 Window
	ct_Window,								// $1a Window
	ct_Door+ct_Window,						// $1b Door - Opened
	0,										// $1c Not-used
	0,										// $1d Not-used
	0,										// $1e Not-used
	0,										// $1f Not-used

	// $20-$2f
	ct_Wall,								// $20 Wall
	ct_Wall,								// $21 Wall
	ct_Wall,								// $22 Wall
	ct_Wall,								// $23 Wall
	ct_Wall+ct_fActive,						// $24 Troch
	ct_Wall,								// $25 Arch
	ct_Wall,								// $26 Arch
	ct_Door+ct_fActive,						// $27 Door - Closed
	ct_Door+ct_fActive,						// $28 Door - Closed
	ct_Door+ct_fActive,						// $29 Door - Closed
	ct_fPass+ct_Door+ct_fActive,			// $2a Door - Closed
	ct_Stairs+ct_fActive,					// $2b Stairs
	ct_Stairs+ct_fActive,					// $2c Stairs
	ct_Stairs+ct_fActive,					// $2d Stairs
	ct_Stairs+ct_fActive,					// $2e Stairs
	ct_Stairs+ct_fActive,					// $2f Stairs

	// $30-$3f
	ct_Stairs+ct_fActive,					// $30 Stairs
	0,										// $31 Not-used
	ct_Stairs+ct_fActive,					// $32 Stairs
	ct_Stairs+ct_fActive,					// $33 Stairs
	0, 										// $34 Not-used
	0, 										// $35 Not-used
	ct_Door+ct_Window,						// $36 Door - Opened
	ct_Door+ct_Window,						// $37 Door - Opened
	ct_Window,								// $38 Window
	ct_Window,								// $39 Window
	ct_Window,								// $3a Window
	ct_Door+ct_Window,						// $3b Door - Opened
	ct_Wall,								// $3c Wall
	ct_Wall,								// $3d Wall
	0,										// $3e Not-used
	ct_fPass+ct_Wall,						// $3f Wall

	// $40-$4f
	ct_fPass+ct_Wall,						// $40 Wall
	ct_fPass+ct_Wall,						// $41 Wall
	ct_fPass+ct_Wall,						// $42 Wall
	ct_fPass+ct_Wall,						// $43 Wall
	ct_fPass+ct_Wall+ct_fActive,			// $44 Troch
	ct_fPass+ct_fActive,					// $45 Switch On
	ct_fPass+ct_fActive,					// $46 Switch Off
	ct_fPass+ct_Door+ct_fActive,			// $47 Door - Closed
	ct_fPass+ct_Wall,						// $48 Column
	ct_fPass+ct_Door+ct_fActive,			// $49 Door - Closed
	ct_fPass+ct_Door+ct_fActive,			// $4a Door - Closed
	ct_fPass+ct_Stairs+ct_Wall+ct_fActive,	// $4b Stairs
	ct_fPass+ct_fActive,					// $4c Stairs
	ct_fPass+ct_Stairs+ct_Wall+ct_fActive,	// $4d Stairs
	ct_fPass+ct_Stairs+ct_Wall+ct_fActive,	// $4e Stairs
	ct_fPass+ct_Stairs+ct_fActive,			// $4f Stairs

	// $50-$5f
	ct_fPass+ct_Stairs+ct_Wall+ct_fActive,	// $50 Stairs
	ct_fPass+ct_Wall,						// $51 Stairs
	ct_fPass+ct_Stairs+ct_Wall+ct_fActive,	// $52 Stairs
	ct_fPass+ct_Stairs+ct_Wall+ct_fActive,	// $53 Stairs
	ct_fPass+ct_Wall,						// $54 Stairs
	ct_fPass+ct_Wall,						// $55 Stairs
	ct_fPass,								// $56 // Tree
	ct_fPass,								// $57 // Tree
	ct_fPass,								// $58 // Tree
	ct_fPass,								// $59 // Tree
	ct_fPass,								// $5a // Stone
	ct_fPass,								// $5b // Stone
	ct_fPass,								// $5c // Stone
	ct_fPass,								// $5d // Stone
	ct_fPass,								// $5e // Stone
	ct_fPass,								// $5f // Stone
	
	// $60-$6f
	0,										// $60 Not-used
	ct_fActive,								// $61 Item Elixir
	ct_fActive,								// $62 Item Gold
	ct_fActive,								// $63 Item Gold
	ct_fActive,								// $64 Item Script
	ct_fActive,								// $65 Item Script
	ct_fActive,								// $66 Item Key
	ct_fActive,								// $67 Item Key
	ct_fActive,								// $68 Item Skull
	ct_fActive,								// $69 Item Shield
	ct_fActive,								// $6a Item Shield
	ct_fActive,								// $6b Item Arrows
	ct_fActive,								// $6c Item Arrows
	ct_fActive,								// $6d Item Pike/Axe
	ct_fActive,								// $6e Item Pike
	ct_fActive,								// $6f Item Axe

	// $70-$7f
	ct_fActive,								// $70 Item Sword
	ct_fActive,								// $71 Item Sword
	ct_fActive,								// $72 Item Bow
	ct_fActive,								// $73 Item Bow
	ct_fActive,								// $74 Item
	ct_fActive,								// $75 Item
	ct_fActive,								// $76 Chest
	ct_fActive,								// $77 Chest
	0,										// $78 Not-used
	0,										// $79 Not-used
	0,										// $7a Not-used
	0,										// $7b Not-used
	0,										// $7c Not-used
	0,										// $7d Not-used
	0,										// $7e Not-used
	0										// $7f Not-used
	);
