# Archon Adventures

A game created for the Atari 8 bit platform based on a game with a similar title "ARCHON", which is a strategy and arcade game intended for the same hardware platform.

Archon Adventures takes us from a strategic battlefield to the world of fantazy, where monsters take over the land in which the game is played. Being chosen from among the three best, we go to clear the land of evil, on the way by completing tasks and (of course) killing evil :)

**DEVELOPMENT INFO:**
A game written using the MadPascal and MadAssembler compiler for 6502 processors (and based on it)

**INITIAL REQUIREMWNTS:**

* ATARI computer or emulator.
* 64KB memory
* Disk drive but not now
* Joystick.

